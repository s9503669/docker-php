# docker-compose-web-installation


## Purpose

This project shows how build a local development environment quickly by docker-compose.


## Getting started

Use command 'docker-compose up -d'

to start **nginx, php, mysql, phpmyadmin** at one time.